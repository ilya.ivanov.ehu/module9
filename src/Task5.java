public class Task5 {
    public static void main(String[] args) {
        var airlines = new Airlines();
        airlines.add(new Airline("Tokyo", 1234, "Boeing 747", new Time(8, 30, 0), 2));
        airlines.add(new Airline("Los Angeles", 5678, "Airbus A320", new Time(12, 15, 0), 5));
        airlines.add(new Airline("Paris", 9012, "Boeing 777", new Time(16, 45, 0), 3));
        airlines.add(new Airline("Tokyo", 3456, "Airbus A330", new Time(20, 30, 0), 3));
        airlines.add(new Airline("Sydney", 7890, "Boeing 787", new Time(6, 0, 0), 1));

        System.out.println(airlines);
        System.out.println();

        System.out.println(airlines.getFlightsByDestination("Tokyo"));
        System.out.println(airlines.getFlightsByDayOfWeek(3));
        System.out.println(airlines.getFlightsByDayOfWeekWithDepartureTime(3, new Time(17, 23, 50)));
    }
}
