import java.util.Arrays;

public class Database {
    private Node head;
    private Node tail;
    private int size;

    public Database() {
        head = null;
        tail = null;
    }

    static class Node {
        private final Student student;
        private Node next;

        public Node(Student student) {
            this.student = student;
            this.next = null;
        }

        public Student getStudent() {
            return student;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

    public boolean isEmpty() {
        return head == null;
    }

    public void add(Student student) {
        Node item = new Node(student);

        if (isEmpty()) {
            head = item;
            tail = item;
            size++;
            return;
        }
        tail.setNext(item);
        tail = item;
        size++;
    }

    public Student find(String surname, String name) {
        Node current = head;
        Student found = null;
        while (current != tail) {
            Student student = current.getStudent();
            if (student.getSurname().equals(surname) && student.getName().equals(name)) {
                found = student;
                break;
            }
            current = current.getNext();
        }
        return found;
    }

    public void remove(String surname, String name) {
        if (isEmpty()) {
            return;
        }

        Node current = head;
        if (current.getStudent().getSurname().equals(surname) && current.getStudent().getName().equals(name)) {
            head = current.getNext();
            size--;
        }
        while (current != tail) {
            Student nextStudent = current.getNext().getStudent();
            if (nextStudent.getSurname().equals(surname) && nextStudent.getName().equals(name)) {
                if (current.getNext().getNext() != null) {
                    current.setNext(current.getNext().getNext());
                    size--;
                    return;
                }
                current.setNext(null);
                tail = current;
                size--;
                return;
            }
            current = current.getNext();
        }
    }

    public void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    public void printDatabase() {
        if (isEmpty()) {
            System.out.println("Database is empty.");
            return;
        }
        Node current = head;
        while (current != null) {
            Student student = current.getStudent();
            System.out.print(student);
            current = current.getNext();
        }
    }

    public int size() {
        return size;
    }

    public Student[] findByMinGrade(int minGrade) {
        Student[] resultArray = new Student[size];
        int arrayIdx = 0;
        if (isEmpty()) {
            return new Student[0];
        }
        Node current = head;
        while (current != null) {
            Student student = current.getStudent();
            int[] grades = student.getGrades();
            if (passFilter(grades, minGrade)) {
                resultArray[arrayIdx] = student;
                arrayIdx++;
            }
            current = current.getNext();
        }

        resultArray = Arrays.copyOf(resultArray, arrayIdx);
        return resultArray;
    }

    private static boolean passFilter(int[] grades, int minGrade) {
        for (int grade: grades) {
            if (grade < minGrade) {
                return false;
            }
        }
        return true;
    }
}


