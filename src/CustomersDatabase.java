import java.util.Arrays;

public class CustomersDatabase {
    private Customer[] customers;
    private int size;

    public CustomersDatabase(Customer[] customers) {
        this.customers = Arrays.copyOf(customers, customers.length);
        this.size = customers.length;
    }

    public CustomersDatabase(int size) {
        this.customers = new Customer[size];
    }

    public Customer[] getCustomers() {
        return customers;
    }

    public int getSize() {
        return size;
    }

    public void add(Customer customer) {
        if (this.customers.length == this.size) {
            this.customers = Arrays.copyOf(this.customers, this.size + 10);
        }
        for (int i = 0; i < customers.length; i++) {
            if (customer.equals(customers[i])) {
                System.out.println("Already in the database.");
                return;
            }
            if (customers[i] == null) {
                customers[i] = customer;
                this.size++;
                return;
            }
        }
    }

    public void sortBySurname() {
        boolean customersSorted = false;
        while(!customersSorted) {
            customersSorted = true;
            for(int i = 1; i < customers.length; i++) {
                if(customers[i].compareTo(customers[i - 1]) < 0) {
                    Customer temp = customers[i];
                    customers[i] = customers[i - 1];
                    customers[i - 1] = temp;
                    customersSorted = false;
                }
            }
        }
    }

    public void sortById() {
        boolean customersSorted = false;
        while(!customersSorted) {
            customersSorted = true;
            for (int i = 1; i < customers.length; i++) {
                if (customers[i].getId() < customers[i - 1].getId()) {
                    Customer temp = customers[i];
                    customers[i] = customers[i - 1];
                    customers[i - 1] = temp;
                    customersSorted = false;
                }
            }
        }
    }
    public Customer[] findByCardRanges(int start, int end) {
        int items = 0;
        Customer[] result = new Customer[this.customers.length];
        for (Customer customer : customers) {
            if (customer != null && customer.getCardNumber() >= start && customer.getCardNumber() <= end) {
                result[items] = customer;
                items++;
            }
        }
        return Arrays.copyOf(result, items);
    }
}



