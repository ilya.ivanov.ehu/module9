public class CustomerMain {
    public static void main(String[] args) {
        Customer smith = new Customer(
                1, "Smith", "Thomas",
                new Address("SW1A 0AA", "London", "Westminster", 23, 25),
                3456, "DE89 3704 0044 0532 0130 00"
        );
        Customer jones = new Customer(
                2, "Jones", "James",
                new Address("SW1A 1AA", "London", "Oxford", 14, 50),
                1234, "DE12 1234 1234 1234 1234 12"
        );
        Customer williams = new Customer(
                10, "Williams", "David",
                new Address("SWC1A 1AA", "London", "High Holborn", 5, 15),
                8901, "DE21 5678 9012 3456 7890 12"
        );
        Customer brown = new Customer(
                7, "Brown", "George",
                new Address("SEC1A 1AA", "London", "Old", 8, 20),
                5678, "DE11 6789 0123 4567 8901 23"
        );
        Customer taylor = new Customer(
                5, "Taylor", "John",
                new Address("SN1A 1AA", "London", "Islington High", 10, 3),
                9012, "DE32 4567 8901 2345 6789 01"
        );
        Customer evans = new Customer(
                6, "Evans", "Michael",
                new Address("LATE RRA", "London", "Kings Cross", 45, 75),
                4567, "DE42 2345 6789 0123 4567 89"
        );
        Customer turner = new Customer(
                4, "Turner", "William",
                new Address("E1W 1AA", "London", "Whitechapel Road", 2, 8),
                0123, "DE53 8901 2345 6789 0123 45"
        );
        Customer lewis = new Customer(
                9, "Lewis", "Richard",
                new Address("SE1P 1AA", "London", "London Bridge", 1, 12),
                6789, "DE64 6789 0123 4567 8901 23"
        );
        Customer green = new Customer(
                8, "Green", "Stephen",
                new Address("SW1B 1AA", "London", "Bond", 6, 18),
                2345, "DE75 4567 8901 2345 6789 01"
        );
        Customer king = new Customer(
                3, "King", "Andrew",
                new Address("SW1F 1AA", "London", "Oxford Circus", 9, 30),
                8901, "DE86 2345 6789 0123 4567 89"
        );

        CustomersDatabase database = new CustomersDatabase(
                new Customer[] {smith, jones, williams, brown, taylor,evans,turner, lewis, green, king}
        );

        System.out.println("Initial array of customers is:");
        Printer1.printDatabase(database);
        System.out.println("-----------------------------------------------------------------------------------------");

        database.sortBySurname();
        System.out.println("Array of customers after sorting by surname is:");
        Printer1.printDatabase(database);
        System.out.println("-----------------------------------------------------------------------------------------");

        System.out.println("Customers with cards in range [4000,8000] are:");
        Customer[] inRange = database.findByCardRanges(4000, 8000);
        Printer1.printCustomers(inRange);
        System.out.println("-----------------------------------------------------------------------------------------");

        System.out.println("Array of customers after sorting by Id is:");
        database.sortById();
        Printer1.printDatabase(database);
        System.out.println("-----------------------------------------------------------------------------------------");

        CustomersDatabase newDatabase = new CustomersDatabase(5);
        System.out.println("New empty array of customers with size 5:");
        Printer1.printDatabase(newDatabase);
        System.out.println("-----------------------------------------------------------------------------------------");

        newDatabase.add(king);
        newDatabase.add(green);

        System.out.println("New array of customers with size 5 after adding 2 customers:");
        Printer1.printDatabase(newDatabase);
    }
}


