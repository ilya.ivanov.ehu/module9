public class Address {
    private String postCode;
    private String city;
    private String street;
    private int building;
    private int apartment;

    public Address(String postCode, String city, String street, int building) {
        this.postCode = postCode;
        this.city = city;
        this.street = street;
        this.building = building;
    }

    public Address(String postCode, String city, String street, int building, int apartment) {
        this.postCode = postCode;
        this.city = city;
        this.street = street;
        this.building = building;
        this.apartment = apartment;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getBuilding() {
        return building;
    }

    public void setBuilding(int building) {
        this.building = building;
    }

    public int getApartment() {
        return apartment;
    }

    public void setApartment(int apartment) {
        this.apartment = apartment;
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s str., bld. %s, apt. %s", postCode, city, street, building, apartment);
    }
}



