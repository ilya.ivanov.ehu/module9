public class Printer1 {
    public static void printDatabase(CustomersDatabase database) {
        Customer[] content = database.getCustomers();
        for (Customer customer: content) {
            System.out.println(customer);
            System.out.println();
        }
    }

    public static void printCustomer(Customer customer) {
        System.out.println(customer);
    }

    public static void printCustomers(Customer[] arrayToPrint) {
        for (Customer customer: arrayToPrint) {
            System.out.println(customer);
            System.out.println();
        }
    }
}



