public class Train {
    private String destination;
    private final int number;
    private Time deptTime;

    public Train(int number) {
        this.number = number;
    }

    public Train(String destination, int number, Time deptTime) {
        this.destination = destination;
        this.number = number;
        this.deptTime = deptTime;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Time getDeptTime() {
        return deptTime;
    }

    public void setDeptTime(int hour, int minute) {
        this.deptTime = new Time(hour, minute);
    }

    public int getNumber() {
        return number;
    }

    public int compareTo(Train other) {
        if (this.destination.compareTo(other.destination) == 0) {
            return this.deptTime.compareTo(other.deptTime);
        }
        return this.destination.compareTo(other.destination);
    }

    @Override
    public String toString() {
        return String.format("Train{destination: %s, number: %d, departure at %s}", destination, number, deptTime);
    }
}



