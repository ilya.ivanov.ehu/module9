import java.util.Arrays;

public class Library {
    private Book[] content;
    private int size;

    public Library(Book[] content) {
        this.content = Arrays.copyOf(content, content.length);
        this.size = content.length;
    }

    public Library(int size) {
        this.content = new Book[size];
    }

    public Book[] getContent() {
        return content;
    }

    public int getSize() {
        return size;
    }

    public void add(Book book) {
        if (this.content.length == this.size) {
            this.content = Arrays.copyOf(this.content, this.size + 10);
        }
        for (int i = 0; i < content.length; i++) {
            if (book.equals(content[i])) {
                System.out.println("Already in the library.");
                return;
            }
            if (content[i] == null) {
                content[i] = book;
                this.size++;
                return;
            }
        }
    }
    public Book[] findByAuthor(String author) {
        int items = 0;
        Book[] result = new Book[this.content.length];
        for (Book book : this.content) {
            if (book != null && book.getAuthor().equals(author)) {
                result[items] = book;
                items++;
            }
        }
        return Arrays.copyOf(result, items);
    }
    public Book[] findByPublisher(String publisher) {
        int items = 0;
        Book[] result = new Book[this.content.length];
        for (Book book : content) {
            if (book != null && book.getPublisher().equals(publisher)) {
                result[items] = book;
                items++;
            }
        }
        return Arrays.copyOf(result, items);
    }
    public Book[] findYoungerThan(int year) {
        int items = 0;
        Book[] result = new Book[this.content.length];
        for (Book book : content) {
            if (book != null && book.getYear() > year) {
                result[items] = book;
                items++;
            }
        }
        return Arrays.copyOf(result, items);
    }
}



