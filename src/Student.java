
    import java.util.Arrays;
    public class Student {
    private final String surname;
    private final String name;
    private int group;
    private int[] grades;

    public Student(String surname, String name, int group) {
        this.surname = surname;
        this.name = name;
        this.group = group;
    }

    public Student(String surname, String name, int group, int[] grades) {
        this.surname = surname;
        this.name = name;
        this.group = group;
        this.grades = grades;
    }

    public String getSurname() {
        return surname;
    }
        public String setSurname() {
            return surname;
        }

    public String getName() {
        return name;
    }
        public String setName() {
            return name;
        }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }


    @Override
    public String toString() {
        return String.format(
                "Student{surname: %s, name: %s, group: %s, grades: %s}\n", surname, name, group, Arrays.toString(grades)
        );
    }
}



