import java.util.Arrays;

public class TrainMain {
    public static void main(String[] args) {
        trainDemo();
    }

    public static void trainDemo() {
        Train[] trains = new Train[6];
        trains[0] = new Train("New York City", 2054, new Time(7, 30));
        trains[1] = new Train("Chicago", 1934, new Time(8, 45));
        trains[2] = new Train("San Francisco", 9813, new Time(10, 15));
        trains[3] = new Train("Houston", 4523, new Time(12, 0));
        trains[4] = new Train("Las Vegas", 5005, new Time(14, 30));
        trains[5] = new Train("Chicago", 5087, new Time(14, 55));

        System.out.println("Initial array of trains is:");
        printTrains(trains);
        System.out.println();

        Train[] sortedByNumber = Arrays.copyOf(trains, trains.length);
        sortByNumber(sortedByNumber);
        System.out.println("Array of trains after sorting by number is:");
        printTrains(sortedByNumber);
        System.out.println();

        Train[] sortedByDestination = Arrays.copyOf(trains, trains.length);
        sortByDestination(sortedByDestination);
        System.out.println("Array of trains after sorting by destination & time is:");
        printTrains(sortedByDestination);
        System.out.println();

        System.out.println("Find train with number 4523:");
        findByNumber(trains, 4523);
        findByNumber(trains, 1020);
    }

    private static void printTrains(Train[] trains) {
        for (Train train: trains) {
            System.out.println(train);
        }
    }

    public static void sortByNumber(Train[] trains) {
        boolean trainsSorted = false;
        while(!trainsSorted) {
            trainsSorted = true;
            for(int i = 1; i < trains.length; i++) {
                if(trains[i].getNumber() < trains[i - 1].getNumber()) {
                    Train temp = trains[i];
                    trains[i] = trains[i - 1];
                    trains[i - 1] = temp;
                    trainsSorted = false;
                }
            }
        }
    }

    public static void sortByDestination(Train[] trains) {
        boolean trainsSorted = false;
        while(!trainsSorted) {
            trainsSorted = true;
            for(int i = 1; i < trains.length; i++) {
                if(trains[i].compareTo(trains[i - 1]) < 0) {
                    Train temp = trains[i];
                    trains[i] = trains[i - 1];
                    trains[i - 1] = temp;
                    trainsSorted = false;
                }
            }
        }
    }

    public static void findByNumber(Train[] trains, int number) {
        for (Train train: trains) {
            if (train.getNumber() == number) {
                System.out.println(train);
                return;
            }
        }
        System.out.printf("No train with number %d found.\n", number);
    }
}



