public class Time {
    private int hour;
    private int minute;
    private int second;

    public Time(int hour, int minute, int second) {
        this.hour = validateHour(hour);
        this.minute = validateMinute(minute);
        this.second = validateSecond(second);
    }

    public Time(int hour, int minute) {
        this.hour = validateHour(hour);
        this.minute = validateMinute(minute);
    }

    private static int validateTime(int givenDigit, int maxAllowedDigit) {
        return givenDigit > maxAllowedDigit? 0: givenDigit;
    }

    private static int validateHour(int hour) {
        return validateTime(hour, 23);
    }

    private static int validateMinute(int minute) {
        return validateTime(minute, 59);
    }

    private static int validateSecond(int second) {
        return validateTime(second, 59);
    }

    public void setTime(int hour, int minute, int second) {
        this.hour = validateHour(hour);
        this.minute = validateMinute(minute);
        this.second = validateSecond(second);
    }

    private void fromSecs(int seconds) {
        int minutes = seconds / 60;
        int hours = seconds / 3600;
        this.second = seconds % 60;
        this.minute = minutes % 60;
        this.hour = hours % 24;
    }

    private int toSecs() {
        int secs = this.second;
        int minutes = this.minute * 60;
        int hours = this.hour * 3600;
        return secs + minutes + hours;
    }



    public int compareTo(Time other) {
        return this.toSecs() - other.toSecs();
    }



    public String toString() {
        return String.format("%02d:%02d:%02d", this.hour, this.minute, this.second);
    }
}



