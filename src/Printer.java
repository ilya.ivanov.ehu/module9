public class Printer {
    public static void printLibrary(Library library) {
        for (Book book : library.getContent()) {
            System.out.println(book);
        }
    }

    public static void printBooks(Book[] booksArray) {
        for (Book book : booksArray) {
            System.out.println(book);
        }
    }

    public static void printBook(Book book) {
        System.out.println(book);
    }
}


